from datetime import timedelta


class Config:
    def __init__(self):
        self.GATHER_NEW_CHANNELS = False        # TODO сеттеры/геттеры для этих двух переменных сделать
        self.GATHER_CHANNELS_STATISTICS = False

        self.YT_SEARCH_DELAY = 20
        self.YT_SEARCH_STRONG_DELAY = 400
        self.YT_GET_INFO_DELAY = 0.01
        self.YT_GET_INFO_STRONG_DELAY = 10
        self.UPDATE_SEARCH_PERIOD = timedelta(days=10)
        self.UPDATE_STATISTICS_PERIOD = timedelta(days=1)

        self.DB_USER = 'gatherer'
        self.DB_PASSWORD = 'gatherer9834'
        self.DB_NAME = 'yt_gatherer'
        self.DB_PORT = '5432'
        self.DB_HOST = 'localhost'

        self.__yt_api_keys = []
        self.get_api_keys()

    # TODO загружать с yml произвольные настройки

    def get_api_keys(self):
        try:
            with open('yt_api_keys.cfg', 'r') as f:
                self.__yt_api_keys = f.readlines()
        except FileNotFoundError:
            with open('yt_api_keys.cfg', 'w'):
                pass
        if not self.__yt_api_keys:
            raise Exception('Нет ни одного API key. Добавьте их в файл "yt_api_keys.cfg".')

    @property
    def YT_APY_KEY(self):
        return self.__yt_api_keys[0]  # TODO добавить возможность переключения при исчерпании лимитов

    @property
    def SQLALCHEMY_URL(self):
        return f'postgresql+psycopg2://{self.DB_USER}:{self.DB_PASSWORD}@{self.DB_HOST}:{self.DB_PORT}/{self.DB_NAME}'


config = Config()
