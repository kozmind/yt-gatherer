import aiohttp

from yt_gatherer.config import config


async def fetch(session, url, params):
    async with session.get(url, params=params) as response:
        answer = await response.json()
        if response.status == 200:
            return answer
        else:
            raise Exception(f'Что-то пошло не так. Youtube ответил кодом: {response.status}\n{answer}')


async def get_statistics_yt_channels(channel_id):
    params = {
        'part': 'statistics',
        'fields': 'items(statistics(subscriberCount,viewCount))',
        'key': config.YT_APY_KEY,
        'id': channel_id
    }
    async with aiohttp.ClientSession() as session:
        answer = await fetch(session, 'https://www.googleapis.com/youtube/v3/channels', params)
    if answer['items']:
        return (
            answer['items'][0]['statistics']['subscriberCount'],
            answer['items'][0]['statistics']['viewCount']
        )
    else:
        return None, None


async def get_new_yt_channels(query, page):
    params = {
        'part': 'snippet',
        'maxResults': 50,
        'fields': 'items(snippet(channelId,channelTitle)),nextPageToken',
        'key': config.YT_APY_KEY,
        'q': query,
    }
    if page:
        params['pageToken'] = page
    async with aiohttp.ClientSession() as session:
        answer = await fetch(session, 'https://www.googleapis.com/youtube/v3/search', params)
        print(answer)
    return answer.get('nextPageToken'), \
        [(item['snippet']['channelId'], item['snippet']['channelTitle']) for item in answer['items']]
