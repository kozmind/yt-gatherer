import asyncio
from sqlalchemy import create_engine, and_, func
from sqlalchemy.orm import sessionmaker
from datetime import datetime

from yt_gatherer.config import config
from yt_gatherer.integration import get_statistics_yt_channels, get_new_yt_channels
from yt_gatherer.models import Statistics, Channel, Word


engine = create_engine(config.sqlalchemy_url)
Session = sessionmaker(bind=engine)
session = Session()


async def yt_channel_ids_gather():
    while config.GATHER_NEW_CHANNELS:
        keyword, current_page = await get_keyword_and_page()
        if keyword is not None:
            print(f'Запрос новых channel_id по ключевику "{keyword}" и странице {current_page}')
            next_page, yt_channels = await get_new_yt_channels(keyword, current_page)
            await save_yt_channels(yt_channels)
            await save_search_state(keyword, next_page)
        else:
            print('По ключевым словам каналы найдены и актуальны. Попробуйте добавить больше ключевых слов.')
            await asyncio.sleep(config.YT_SEARCH_STRONG_DELAY)
        await asyncio.sleep(config.YT_SEARCH_DELAY)
    print('Сбор новых channel_id остановлен')


async def yt_statistics_gather():
    while config.GATHER_CHANNELS_STATISTICS:
        channel_id = await get_channel_id()
        if channel_id is None:
            print('пока нет каналов для сбора статистики')
            await asyncio.sleep(config.YT_GET_INFO_STRONG_DELAY)
            continue
        print(f'запрос информации о канале {channel_id}')
        subscribers_count, views_count = await get_statistics_yt_channels(channel_id)
        if subscribers_count is not None:
            await save_statistics_yt_channels(channel_id, subscribers_count, views_count)
        else:
            await disable_channel(channel_id)
            print(f'канал {channel_id} помечен как неактивный')
        await asyncio.sleep(config.YT_GET_INFO_DELAY)
    print('Сбор статистики остановлен')


async def get_channel_id():
    channel_id, channel_update_time = session.query(Channel.id, func.max(Statistics.add_time))\
        .filter(Channel.available.isnot(False))\
        .join(Statistics, Channel.id == Statistics.channel_id, isouter=True)\
        .group_by(Channel.id)\
        .order_by(func.max(Statistics.add_time).desc())\
        .first()
    if channel_update_time is not None:
        if (datetime.now(tz=None) - channel_update_time) < config.UPDATE_STATISTICS_PERIOD:
            return None
    return channel_id


async def save_statistics_yt_channels(channel_id, subscribers_count, views_count):
    session.add(Statistics(
        channel_id=channel_id,
        subscribers_count=subscribers_count,
        views_count=views_count,
        add_time=datetime.now(tz=None),
    ))
    session.commit()


async def get_keyword_and_page():
    word = session.query(Word).filter(and_(Word.page_token.isnot(None), Word.update_time.isnot(None))).first()
    if word is None:
        word = session.query(Word).filter(Word.update_time.is_(None)).first()
    if word is None:
        word = session.query(Word).filter((datetime.now() - Word.update_time) > config.UPDATE_SEARCH_PERIOD).first()
    return (word.word, word.page_token) if word else (None, None)


async def save_yt_channels(yt_channels):
    for channel in yt_channels:
        channel_in_db = session.query(Channel).filter(Channel.id == channel[0]).one_or_none()
        if not channel_in_db:
            session.add(Channel(id=channel[0], name=channel[1]))
        else:
            channel_in_db.name = channel[1]
    session.commit()


async def save_search_state(keyword, next_page):
    word_in_db = session.query(Word).filter(Word.word == keyword).one_or_none()
    if word_in_db:
        word_in_db.page_token = next_page
        word_in_db.update_time = datetime.now(tz=None)
    else:
        session.add(Word(word=keyword, page_token=next_page))
    session.commit()


async def disable_channel(channel_id):
    channel = session.query(Channel).filter(Channel.id == channel_id).one()
    channel.available = False
    session.commit()
