import asyncio
from aiohttp import web

from yt_gatherer.config import config
from yt_gatherer.controllers import yt_statistics_gather, yt_channel_ids_gather


async def start_gather_statistics(request):
    if not config.GATHER_CHANNELS_STATISTICS:
        config.GATHER_CHANNELS_STATISTICS = True
        asyncio.get_event_loop().create_task(yt_statistics_gather())
        return web.Response(text='запустился сбор статистики')
    return web.Response(text='уже запущен сбор статистики')


async def start_gather_new_channels(request):
    if not config.GATHER_NEW_CHANNELS:
        config.GATHER_NEW_CHANNELS = True
        asyncio.get_event_loop().create_task(yt_channel_ids_gather())
        return web.Response(text='запустился поиск каналов')
    return web.Response(text='уже запущен поиск каналов')


async def stop_gather_statistics(request):
    if config.GATHER_CHANNELS_STATISTICS:
        config.GATHER_CHANNELS_STATISTICS = False
        return web.Response(text='остановлен сбор статистики')
    return web.Response(text='не запущен сбор статистики')


async def stop_gather_new_channels(request):
    if config.GATHER_NEW_CHANNELS:
        config.GATHER_NEW_CHANNELS = False
        return web.Response(text='остановлен поиск новых каналов')
    return web.Response(text='не запущен поиск новых каналов')