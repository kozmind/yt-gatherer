from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, BigInteger, String, DateTime, ForeignKey, Boolean
from sqlalchemy.orm import relationship


Base = declarative_base()


class Channel(Base):
    __tablename__= 'channels'

    id = Column(String, primary_key=True)
    name = Column(String)
    available = Column(Boolean, default=True)


class Statistics(Base):
    __tablename__ = 'statistics'

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    add_time = Column(DateTime)
    channel_id = Column(String, ForeignKey('channels.id'))
    channel = relationship('Channel')
    subscribers_count = Column(BigInteger)
    views_count = Column(BigInteger)


class Word(Base):
    __tablename__ = 'words'

    id = Column(Integer, primary_key=True, autoincrement=True)
    word = Column(String, primary_key=True)
    page_token = Column(String)
    update_time = Column(DateTime)

