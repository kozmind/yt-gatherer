"""fill words

Revision ID: b6bb91d5b4d5
Revises: e744be3f9ccb
Create Date: 2019-07-28 21:35:31.808371

"""
from alembic import op
import sqlalchemy as sa

from yt_gatherer.models import Base


# revision identifiers, used by Alembic.
revision = 'b6bb91d5b4d5'
down_revision = 'e744be3f9ccb'
branch_labels = None
depends_on = None


def upgrade():
    op.bulk_insert(
        Base.metadata.tables['words'],
        [
            {'word': 'prank'},
            {'word': 'joke'},
            {'word': 'news'},
            {'word': 'fitness'},
            {'word': 'sport'},
            {'word': 'gym'},
            {'word': 'cook'},
        ],
    )


def downgrade():
    pass
