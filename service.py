from aiohttp import web

from yt_gatherer.views import (
    start_gather_new_channels,
    start_gather_statistics,
    stop_gather_new_channels,
    stop_gather_statistics,
)

if __name__ == '__main__':

    app = web.Application()
    app.add_routes(
        [
            web.get('/stat/stop', stop_gather_statistics),
            web.get('/stat/start', start_gather_statistics),
            web.get('/new/stop', stop_gather_new_channels),
            web.get('/new/start', start_gather_new_channels),
        ]
    )

    print('\nЗапустите нужные процессы по ручкам:\n'
          'GET /new/start - старт поиска новых каналов\n'
          'GET /stat/start - старт сбора статистики\n'
          'GET /new/stop - остановить поиск новых каналов\n'
          'GET /stat/stop - остановить сбор статистики\n')

    web.run_app(app)

